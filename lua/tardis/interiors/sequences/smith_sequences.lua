--11th TARDIS interior - Control sequences (advanced mode)

local Seq = {
	ID = "smith_sequences",

	["dtsmithkeyboard"] = {
		Controls = {
			"dtsmithsliders",
			"dtsmithlevers",
			"dtsmithgears",
			"dtsmithlever4",
			"dtsmithcranks2",
			"dtsmithlever",
			"dtsmithlever2",
			"dtsmithhandbrake",
			"dtsmiththrottle"
		},
		OnFinish = function(self, ply, step, part)
			if IsValid(self) and IsValid(self) then
				self.exterior:Demat()
			end
		end,
		OnFail = function(self, ply, step, part)
			-- fail stuff
		end
	},

	["dtsmithtelepathic"] = {
		Controls = {
			"dtsmithsliders",
			"dtsmithlevers",
			"dtsmithgears",
			"dtsmithlever4",
			"dtsmithcranks2",
			"dtsmithlever",
			"dtsmithlever2", 
			"dtsmithhandbrake",
			"dtsmiththrottle",
		},
		OnFinish = function(self, ply, step, part)
			if IsValid(self) and IsValid(self) then
				self.exterior:Demat()
			end
		end,
		OnFail = function(self, ply, step, part)
			-- fail stuff
		end
	}
}

TARDIS:AddControlSequence(Seq)