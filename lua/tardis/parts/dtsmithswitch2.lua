local PART={}
PART.ID = "dtsmithswitch2"
PART.Name = "dtsmithswitch2"
PART.Model = "models/doctorwho1200/toyota/switch2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
if SERVER then
	function PART:Use( ply )
	local exterior=self.exterior
	local extdoors=exterior:GetPart("door")
		if ( self:GetOn() ) then
		self:EmitSound( Sound( "doctorwho1200/toyota/switch2.wav" ))
		ply:ChatPrint("TARDIS Cloaking Device Deactivated")
		exterior:SetColor (Color(0,191,255,55))
		extdoors:SetColor (Color(0,191,255,55))
		exterior:EmitSound( Sound( "doctorwho1200/toyota/uncloak.wav" ))
			timer.Create( "uncloaktimer" , 1 , 1 , function()
			exterior:SetColor (Color(255,255,255,255))
			exterior:SetRenderMode( RENDERMODE_NORMAL )
			extdoors:SetColor (Color(255,255,255,255))
			extdoors:SetRenderMode( RENDERMODE_NORMAL )
			end )
		else
		self:EmitSound( Sound( "doctorwho1200/toyota/switch2.wav" ))
		ply:ChatPrint("TARDIS Cloaking Device Activated")
		exterior:SetRenderMode( RENDERMODE_TRANSALPHA )
		exterior:SetColor (Color(0,191,255,55))
		extdoors:SetRenderMode( RENDERMODE_TRANSALPHA )
		extdoors:SetColor (Color(0,191,255,55))
		exterior:EmitSound( Sound( "doctorwho1200/toyota/cloak.wav" ))
			timer.Create( "cloaktimer" , 1 , 1 , function()
			exterior:SetRenderMode( RENDERMODE_TRANSALPHA )
			exterior:SetColor (Color(255,255,255,0))
			extdoors:SetRenderMode( RENDERMODE_TRANSALPHA )
			extdoors:SetColor (Color(255,255,255,0))

			end )
		end
 	end
end

TARDIS:AddPart(PART,e)