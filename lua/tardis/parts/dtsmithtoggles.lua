local PART={}
PART.ID = "dtsmithtoggles"
PART.Name = "dtsmithtoggles"
PART.Model = "models/doctorwho1200/toyota/toggles.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "doctorwho1200/toyota/toggles.wav"

function PART:Use(ply)
    if ply~=self.interior:GetCreator() then
        ply:ChatPrint("This is not your TARDIS")
        return
    end
    if self.interior:ToggleSecurity() then
        ply:ChatPrint("Isomorphic ".. (self.interior:GetSecurity() and "engaged" or "disengaged"))
    end
end
TARDIS:AddPart(PART)