local PART={}
PART.ID = "dtsmithlever5"
PART.Name = "dtsmithlever5"
PART.Model = "models/doctorwho1200/toyota/lever5.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
if SERVER then
	function PART:Use()
		self.exterior:ToggleFastRemat()
		if ( self:GetOn() ) then
			self:EmitSound( Sound( "doctorwho1200/toyota/lever5.wav" ))
		else
			self:EmitSound( Sound( "doctorwho1200/toyota/lever5.wav" ))
		end
	end
end

TARDIS:AddPart(PART,e)