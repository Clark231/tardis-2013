local PART={}
PART.ID = "dtsmithcrank2"
PART.Name = "dtsmithcrank2"
PART.Model = "models/doctorwho1200/toyota/crank2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
if SERVER then
	function PART:Use()
		if ( self:GetOn() ) then
			self:EmitSound( Sound( "doctorwho1200/toyota/crank2.wav" ))
			self.exterior:ToggleRepair()
		else
			self:EmitSound( Sound( "doctorwho1200/toyota/crank2.wav" ))
			self.exterior:ToggleRepair()
		end
	end
end

TARDIS:AddPart(PART,e)